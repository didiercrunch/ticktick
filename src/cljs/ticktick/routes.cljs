(ns ticktick.routes
  (:require-macros [secretary.core :refer [defroute]])
  (:import [goog History]
           [goog.history EventType])
  (:require
    [secretary.core :as secretary]
    [goog.events :as gevents]
    [re-frame.core :as re-frame]
    [ticktick.events :as events]))


(defn hook-browser-navigation! []
  (doto (History.)
    (gevents/listen
      EventType/NAVIGATE
      (fn [event]
        (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

(defn app-routes []
  (secretary/set-config! :prefix "#")
  ;; --------------------
  ;; define routes here
  (defroute "/" []
            (re-frame/dispatch [::events/set-active-panel :home-panel]))

  (defroute "/now" []
            (re-frame/dispatch [::events/set-active-panel :now-panel]))

  (defroute "/utc" []
          (re-frame/dispatch [::events/set-active-panel :utc-panel]))

  (defroute "/custom" []
            (re-frame/dispatch [::events/set-active-panel :custom-panel])))


  ;; --------------------
(hook-browser-navigation!)
