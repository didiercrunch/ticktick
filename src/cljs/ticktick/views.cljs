(ns ticktick.views
  (:require
   [re-frame.core :as re-frame]
   [ticktick.subs :as subs]
   [ticktick.components.menus :refer [side-menu top-bar]]
   [ticktick.components.clocks :refer [clock]]
   [ticktick.components.digital-clocks :refer [editable-digital-clock]]))

;; home

(defn home-panel []
  [:div.columns.is-centered
   [:div.column.has-text-centered
    [:h1.title "Clocks are amazing!"]]])

;; about

(defn now-panel []
  (let [offset @(re-frame/subscribe [::subs/local-time-zone-offset])]
    [:div
     [:div.columns.is-centered
      [:div.column.has-text-centered
       [:br]
       [clock ::subs/now offset]]]]))

(defn utc-panel []
  [:div
   [:div.columns.is-centered
    [:div.column.has-text-centered
     [:br]
     [clock ::subs/now 0]]]])

(defn custom-panel []
  [:div
   [:div.columns.is-centered
    [:div.column.has-text-centered
     [:br]
     [editable-digital-clock ::subs/custom 0]
     [clock ::subs/custom 0]]]])

(defn error-404 []
  [:h1 "error 404"])

;; main

(defn- panels [panel-name]
  (case panel-name
    :home-panel [home-panel]
    :now-panel [now-panel]
    :utc-panel [utc-panel]
    :custom-panel [custom-panel]
    [error-404]))

(defn show-panel [panel-name]
  [:div
   [top-bar]
   [:div.columns.is-centered
    [:div.column.has-text-centered.is-half-desktop
     [panels panel-name]]
    [:div.column.is-one-quarter-desktop (side-menu)]]])

(defn main-panel []
  (let [active-panel (re-frame/subscribe [::subs/active-panel])]
    [show-panel @active-panel]))
