(ns ticktick.events
  (:require
   [re-frame.core :as re-frame]
   [ticktick.db :as db]))


(re-frame/reg-event-db
 ::initialize-db
 (fn [_ _]
   db/default-db))

(re-frame/reg-event-db
  ::set-active-panel
  (fn [db [_ active-panel]]
    (assoc db :active-panel active-panel)))

(re-frame/reg-event-db
  ::heart-beat
  (fn [db _]
    (let [one-day (* 24 60 60 1000)
          now (.now js/Date)]
      (assoc db :now (mod now one-day)))))

(defn- contains?-in [coll path]
  (if (empty? path)
    true
    (if (contains? coll (first path))
      (recur (get coll (first path)) (rest path))
      false)))

(re-frame/reg-event-db
  :editable-cell-field/init-once
  (fn [db [_ path value validator]]
    (if (not (contains?-in db path))
      (let [validator (if (nil? validator) validator (constantly true))
            default {:visited? false
                     :active? false
                     :initial-value value
                     :value value
                     :valid? (validator value)
                     :validator validator}]
        (assoc-in db path default))
      db)))

(re-frame/reg-event-db
  :editable-cell-field/visit
  (fn [db [_ path]]
    (let [db (assoc-in db (conj path :visited?) true)
          db (assoc-in db (conj path :active?) true)]
      db)))

(re-frame/reg-event-db
  :editable-cell-field/leave
  (fn [db [_ path]]
    (println (db :global-conf))
    (let [{value :value} (get-in db path)]
      (assoc-in db (conj path :value) value))))

(re-frame/reg-event-db
  :editable-cell-field/change
  (fn [db [_ path value]]
    (let [validator (or (get-in db (conj path :validator) (constantly true)))
          db (assoc-in db (conj path :value) value)
          db (assoc-in db (conj path :valid?) (validator value))]
      db)))
