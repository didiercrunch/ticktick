(ns ticktick.db)

(def default-db
  {:name "re-frame"
   :local-time-zone-offset (* (. (new js/Date) getTimezoneOffset) 60 1000)
   :global-conf {}})

