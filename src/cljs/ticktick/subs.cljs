(ns ticktick.subs
  (:require
   [re-frame.core :as re-frame]))

(re-frame/reg-sub
 ::name
 (fn [db]
   (:name db)))

(re-frame/reg-sub
 ::active-panel
 (fn [db _]
   (:active-panel db)))


(re-frame/reg-sub
  ::now
  (fn [db]
    (:now db)))

(re-frame/reg-sub
  ::local-time-zone-offset
  (fn [db]
    (:local-time-zone-offset db)))


(re-frame/reg-sub
  ::get-value
  (fn [db [_ path]]
    (get-in db path)))

(re-frame/reg-sub
  ::custom
  (fn [db]
    (let [hours (get-in db [:editable-digital-clocks :id :hours :value])
          minutes (get-in db [:editable-digital-clocks :id :minutes :value])
          seconds (get-in db [:editable-digital-clocks :id :seconds :value])
          time-zone-offset (get db :time-zone-offset)]
      (+ time-zone-offset
        (* 1000 60 60 hours)
        (* 1000 60 minutes)
        (* 1000 seconds)))))

(re-frame/reg-sub
  ::edited-value
  (fn [db [_ path]]
    (let [value (get-in db path)]
      [(:value value) (:valid? value)])))

(re-frame/reg-sub
  ::changed?-value
  (fn [db [_ path]]
    (let [value (get-in db (conj path :value))
          initial-value (get-in db (conj path :initial-value))]
      (not= value initial-value))))

