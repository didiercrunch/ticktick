(ns ticktick.components.digital-clocks
  (:require [re-frame.core :as re-frame]
            [ticktick.components.util :refer [decompose-time]]))

(defn get-editable-cell-class [changed? valid?]
  (when changed?
    (if valid? "is-success" "is-danger")))

(defn editable-cell-in-edited-mode [path original-value [value valid?] changed? viewer classes styles]
  [:span
   [:input
    {:type        "text"
     :class       classes
     :style       styles
     :value       (viewer value)
     :placeholder (str original-value)
     :on-focus    #(re-frame/dispatch [:editable-cell-field/visit path])
     :on-change   #(re-frame/dispatch [:editable-cell-field/change path (-> % .-target .-value)])
     :on-blur     #(re-frame/dispatch [:editable-cell-field/leave path])}]])

(defn editable-cell [original-value path validator viewer classes styles]
  (let [value (re-frame/subscribe [:ticktick.subs/edited-value path])
        changed? (re-frame/subscribe [:ticktick.subs/changed?-value path])]
    (re-frame.core/dispatch [:editable-cell-field/init-once path original-value validator])
    #(editable-cell-in-edited-mode path original-value @value @changed? viewer classes styles)))

(defn editable-invisible-input [original-value path validator viewer]
  (let [classes ["s-family-monospace"]
        styles {:border           "none"
                :width            "1.3333333333333em"
                :font-style       "inherit"
                :font-weight      "inherit"
                :font-size        "inherit"
                :background-color :transparent}]
    (editable-cell original-value path validator viewer classes styles)))


(defn invisible-input [value]
  [:input.is-family-monospace
   {:style {:border           "none"
            :width            "1.3333333333333em"
            :font-style       "inherit"
            :font-weight      "inherit"
            :font-size        "inherit"
            :background-color :transparent}
    :value value
    :type  "text"}])

(defn lpad [s char tot-length]
  (if (>= (count (str s)) tot-length)
    s
    (recur (str char s) char tot-length)))

(defn strip-left [s char]
  (if (and (string? s) (= (first s) char))
    (recur (. s substr 1) char)
    s))

(defn editable-digital-clock [time-query offset]
  (let [[hours minutes seconds] (decompose-time @(re-frame/subscribe [time-query]) offset)
        validator #(and (pos-int? %) (< % 60))
        viewer (comp #(lpad % "0" 2) #(strip-left % "0"))]
    [:div.is-family-monospace
     [editable-invisible-input (lpad hours "0" 2) [:editable-digital-clocks :id :hours] validator viewer]
     [:span {:key "sep-1"} ":"]
     [editable-invisible-input (lpad minutes "0" 2) [:editable-digital-clocks :id :minutes] validator viewer]
     [:span {:key "sep-2"} ":"]
     [editable-invisible-input (lpad seconds "0" 2) [:editable-digital-clocks :id :seconds] validator viewer]]))
