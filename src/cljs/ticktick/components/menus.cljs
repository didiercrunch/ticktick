(ns ticktick.components.menus
  (:require [re-frame.core :as re-frame]))

(defn top-bar []
  [:nav.navbar {:role "navigation" :aria-label "main navigation"}
   [:div.navbar-brand
    [:a.navbar-item {:href "#/"}
     [:img {:src "https://image.flaticon.com/icons/png/512/40/40204.png" :width "25" :height "28"}]]]
   [:div#navbarBasicExample.navbar-menu
    [:div.navbar-start
     [:a.navbar-item {:href "#/now" :key :local-time-zone} "Local time zone"]
     [:a.navbar-item {:href "#/utc" :key :utc} "UTC"]
     [:a.navbar-item {:href "#/custom" :key :custom} "Manual"]]
    [:div.navbar-end
     [:div.navbar-item
      [:div.buttons
       [:a.button.is-primary
        [:strong "Share"]]
       [:a.button.is-light "View"]]]]]])


(defn slide-option [text id]
  (let [[val _] @(re-frame/subscribe [:ticktick.subs/edited-value (conj id :value)])]
    [:div.field
     [:input.switch.is-thin
      {:type      "checkbox"
       :name      "switchExample"
       :checked   val
       :on-change #(re-frame/dispatch [:editable-cell-field/change id (-> % .-target .-checked)])
       :on-blur   #(re-frame/dispatch [:editable-cell-field/leave id])
       :id        id}]
     [:label {:for id} text]]))


(defn side-menu []
  [:aside.menu
   [:p.menu-label "Appearances"]
   [:ul.menu-list
    [:li [:a "Show Hands"]
     [:ul
      [:li [slide-option "Seconds" [:global-conf :seconds-hand]]]
      [:li [slide-option "Minutes" [:global-conf :minutes-hand]]]
      [:li [slide-option "Hours" [:global-conf :hours-hand]]]]]]
   [:ul.menu-list
    [:li [:a "Show Ticks"]
     [:ul
      [:li [slide-option "Seconds" [:global-conf :seconds-tick]]] ;; :global-conf :seconds-hand :value
      [:li [slide-option "Minutes" [:global-conf :minutes-tick]]]]]]])

