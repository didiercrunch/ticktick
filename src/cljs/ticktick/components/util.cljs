(ns ticktick.components.util)


(defn adjust-for-timezone [unix offset]
  (let [ts (. (new js/Date unix) getTimezoneOffset)
        dt (* 60 1000 (- ts offset))]
    (+ unix dt)))

(defn decompose-time [unix offset]
  (let [d (new js/Date (adjust-for-timezone unix offset))]
    [(.getHours d) (.getMinutes d) (.getSeconds d)]))
