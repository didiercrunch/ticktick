(ns ticktick.components.clocks
  (:require [re-frame.core :as re-frame]
            [ticktick.subs :as subs]
            [ticktick.components.util :refer [decompose-time]]))

(def golden-ratio 1.61803)


(defn radius [[width height]]
  (Math/sqrt (+
               (* (/ width 2) (/ width 2))
               (* (/ height 2) (/ height 2)))))

(defn central [[width height]]
  (let [cx (/ width 2)
        cy (/ height 2)
        r (/ (radius [width height]) (* golden-ratio 20))]
    [:circle {:cx cx :cy cy :r r :fill :black}]))

(defn svg-rotate
  ([a x y]
   [:rotate a x y])
  ([a]
   [:rotate a]))

(defn svg-translate [x y]
  [:translate x y])

(defn svg-dsl->attr [[fn-name & fn-attrs]]
  (str (name fn-name) "(" (clojure.string/join " " fn-attrs) ")"))

(defn svg-fn->attr [& svg-fns]
  (loop [ret "" svg-fn (first svg-fns) svg-fns (rest svg-fns)]
    (let [attr (str ret (svg-dsl->attr svg-fn))]
      (if (empty? svg-fns)
        attr
        (recur (str attr " ") (first svg-fns) (rest svg-fns))))))

(defn clock->svg [deg]
  (- deg 90))

(defn hour-hand-angle [[hours minutes second]]
  (let [one-hour-in-deg (/ 360 12)]
    (+ (* hours one-hour-in-deg)
       (* (/ minutes 60) one-hour-in-deg)
       (* (/ second 60 60) one-hour-in-deg))))

(defn minute-hand-angle [[_ minutes seconds]]
  (let [one-minute-in-deg (/ 360 60)]
    (+ (* minutes one-minute-in-deg)
       (* (/ seconds 60) one-minute-in-deg))))


(defn second-hand-angle [[_ _ seconds]]
  (let [one-second-in-deg (/ 360 60)]
    (* one-second-in-deg seconds)))

(defn plot-polar-rect [[tot-width tot-height] angle content]
  (let [cx (/ tot-width 2)
        cy (/ tot-height 2)
        transform (svg-fn->attr
                    (svg-translate cx (+ cy))
                    (-> angle clock->svg (svg-rotate 0 0)))]
    [:g {:transform transform}
     content]))

(defn hour-hand [canvas time]
  (let [angle (hour-hand-angle time)
        rad (radius canvas)
        width (/ rad 3)
        height (/ rad 20)]
    (plot-polar-rect canvas angle
                     [:rect {:width width :height height :y (/ height -2) :style {:fill :gray :rx 5}}])))

(defn minute-hand [canvas time]
  (let [angle (minute-hand-angle time)
        rad (radius canvas)
        width (/ rad 2)
        height (/ rad 20)]
    (plot-polar-rect canvas angle
                     [:rect {:width width :height height :y (/ height -2) :style {:fill :gray :rx 5}}])))


(defn second-hand [canvas time]
  (let [angle (second-hand-angle time)
        rad (radius canvas)
        width (/ rad golden-ratio)
        height (/ rad 20)]
    (plot-polar-rect canvas angle
                     [:rect {:width width :height height :y (/ height -2) :style {:fill :gray :rx 5}}])))


(defn minute-tick [canvas time]
  (let [angle (minute-hand-angle time)
        rad (radius canvas)
        x (/ rad golden-ratio)
        width (/ rad 20)
        height (/ rad 30)]
    (plot-polar-rect canvas angle
                     [:rect {:width width :height height :y (/ height -2) :x x :style {:fill :pink}}])))


(defn second-tick [canvas time]
  (let [angle (minute-hand-angle time)
        rad (radius canvas)
        width (/ rad 30)
        x (+ (/ rad golden-ratio) (- (/ rad 20) width))
        height (/ rad 75)]
    (plot-polar-rect canvas angle
                     [:rect {:width width :height height :y (/ height -2) :x x :style {:fill :lightpink}}])))

(defn second-ticks [canvas minutes-ticks-shown]
  (if minutes-ticks-shown
    (for [min (filter #(not= (mod % 5) 0) (range 60))] (second-tick canvas [0 min]))
    (for [min (range 60)] (second-tick canvas [0 min]))))

(defn minute-ticks [canvas]
  (for [min (map #(* 5 %) (range 12))] (minute-tick canvas [0 min])))


(defn clock [time-query offset]
  (let [x  @(re-frame/subscribe [time-query])
        show-seconds @(re-frame/subscribe [:ticktick.subs/get-value [:global-conf :seconds-tick :value]])
        show-minutes @(re-frame/subscribe [:ticktick.subs/get-value [:global-conf :minutes-tick :value]])
        show-hours-hand @(re-frame/subscribe [:ticktick.subs/get-value [:global-conf :hours-hand :value]])
        show-minutes-hand @(re-frame/subscribe [:ticktick.subs/get-value [:global-conf :minutes-hand :value]])
        show-seconds-hand @(re-frame/subscribe [:ticktick.subs/get-value [:global-conf :seconds-hand :value]])
        time (decompose-time x offset)
        [width height :as canvas] [400 400]]
    [:div.container
     [:svg {:height width :width height}
      (when show-minutes (minute-ticks canvas))
      (when show-seconds (second-ticks canvas show-minutes))
      (when show-seconds-hand (second-hand canvas time))
      (when show-minutes-hand (minute-hand canvas time))
      (when show-hours-hand (hour-hand canvas time))
      (central canvas)]]))
