(ns ticktick.core
  (:require
   [reagent.core :as reagent]
   [reagent.dom :as rdom]
   [re-frame.core :as re-frame]
   [ticktick.events :as events]
   [ticktick.routes :as routes]
   [ticktick.views :as views]
   [cljs.core.async :refer [<! timeout] :refer-macros [go-loop]]
   [ticktick.config :as config]))



(defn dev-setup []
  (when config/debug?
    (println "dev mode")))

(defn ^:dev/after-load mount-root []
  (re-frame/clear-subscription-cache!)
  (let [root-el (.getElementById js/document "app")]
    (rdom/unmount-component-at-node root-el)
    (rdom/render [views/main-panel] root-el)))

(defn dispatch-ticks [n]
  (go-loop [ch  (timeout n)]
           (<! ch)
           (re-frame/dispatch [::events/heart-beat])
           (recur (timeout n))))

(defn init []
  (routes/app-routes)
  (re-frame/dispatch-sync [::events/initialize-db])
  (dispatch-ticks 100)
  (dev-setup)
  (mount-root))
